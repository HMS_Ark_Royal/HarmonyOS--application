import router from '@system.router';
import ability_featureAbility from '@ohos.ability.featureAbility';
import data_storage from '@ohos.data.storage';

export default {
    data: {
        group:'默认分组',
        title:'一篇笔记',
        notebook_stamp:'0',
        note_key:'0',

        editor_content:``,
        is_edit:false,                   //编辑状态参量，为true时代表正在编辑笔记
        my_storage:null,
        dir:'',
        path:'',
    },
    async onInit(){
        //建立轻量级偏好数据库存储接口
        var context = ability_featureAbility.getContext();
        this.dir = await context.getFilesDir();
        this.path = this.dir + '/' + this.notebook_stamp;
        this.my_storage = data_storage.getStorageSync(this.path);
        //从数据库中恢复数据
        if(this.my_storage.hasSync(this.note_key)){
            let value = this.my_storage.getSync(this.note_key,'default');
            this.editor_content = value;
        }else{
            this.editor_content = '大道至简，记录不凡';
        }
    },
    setStorage() {            //将当前笔记内容保存至storage中
        this.my_storage.putSync(this.note_key,this.editor_content);
        this.my_storage.flushSync();
    },
    onBackPress(){
        if(this.is_edit){
            this.end_edit();
        }else{
            this.set_storage();
            router.back();
        }
        return true;
    },
    into_edit(){
        this.is_edit = true;
    },
    get_content(e){
        this.editor_content = e.value;
        this.setStorage();
    },
    end_edit(){
        this.is_edit = false;
        this.setStorage();
    },


}
