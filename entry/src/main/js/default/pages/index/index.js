import prompt from '@system.prompt';
import device from '@system.device';
import mediaquery from '@system.mediaquery';

const TAG = '[index]';
//var mMediaQueryList;
//var context;

// js call java
// abilityType: 0-Ability; 1-Internal Ability
//const ABILITY_TYPE_EXTERNAL = 0;
//const ABILITY_TYPE_INTERNAL = 1;
// syncOption(Optional, default sync): 0-Sync; 1-Async
//const ACTION_SYNC = 0;
//const ACTION_ASYNC = 1;
//const ACTION_MESSAGE_CODE = 1001;

//var wearableMediaListener = function (e) {
//    if (e.matches) {
//        // do something
//        console.log(TAG + "Success Media Listen");
//        context.initial_value = 4;
//        context.$child('id_fragment_main').initial_index_value = 0;
//        context.$child('id_fragment_main').list_data.forEach(element => {
//            element.item_icon = context.images_resource_dark_mode.image_icon;
//            element.item_right_arrow = context.images_resource_dark_mode.image_right_arrow;
//        });
//    }
//};

var getDeviceInfo = function () {  //模板自带功能函数，会导致保错，但删除后会使得页面显示异常
    var res = '';
    device.getInfo({
        success: function (data) {
            console.log(TAG + 'Success device obtained. screenShape=' + data.screenShape);
            this.res = data.screenShape;
        },
        fail: function (data, code) {
            console.log(TAG + 'Failed to obtain device. Error code=' + code + '; Error information: ' + data);
        },
    });
    return res;
};


export default {
    data: {
        initial_value: 0,
        multi_select_mode:false,      //多选状态参量，为true时进入多选界面
    },
    onInit() {
        console.log(TAG + 'onInit');
//        context = this;
//        this.mMediaQueryList = mediaquery.matchMedia("screen and (device-type: wearable)");
//        this.mMediaQueryList.addListener(wearableMediaListener);
        //console.info(TAG + "java js" + this.getSystemColorModeByJava()); // async call and return null
    },
    onReady() {
        console.log(TAG + 'onReady');
        console.log(TAG + getDeviceInfo()); // getDeviceInfo after Init
    },
    onShow() {
        console.log(TAG + 'onShow');
    },
    onBackPress(){
        if(this.multi_select_mode){
            this.end_multi_select();       //多选状态下返回结束多选
            return true;
        }
    },
    onDestroy() {
        console.log(TAG + 'onDestroy');
//        mMediaQueryList.removeListener(wearableMediaListener);
    },

    into_multi_select(){                  //经fragment_main传入的notebook_item长按事件：进入多选状态
        this.multi_select_mode = true;    //置位多选状态参量为true
    },
    end_multi_select(){
        this.multi_select_mode = false;   //结束多选：复位多选状态参量为false
    },

//    getSystemColorModeByJava: async function () {
//        var actionData = {};
//        actionData.firstNum = 123;
//        actionData.secondNum = 465;
//
//        var action = {};
//        action.bundleName = 'com.lexington.simplenote';
//        action.abilityName = 'com.lexington.simplenote.ServiceAbilityForJS';
//        action.messageCode = ACTION_MESSAGE_CODE;
//        action.data = actionData;
//        action.abilityType = ABILITY_TYPE_EXTERNAL;
//        action.syncOption = ACTION_SYNC;
//
//        var result = await FeatureAbility.callAbility(action);
//        var ret = JSON.parse(result);
//        if (ret.code == 0) {
//            console.info('result is:' + JSON.stringify(ret.abilityResult));
//        } else {
//            console.error('error code:' + JSON.stringify(ret.code));
//        }
//
//        if (ret.getColorMode == 0) {
//            console.info(TAG + ret.getColorMode);
//        }
//    }
}