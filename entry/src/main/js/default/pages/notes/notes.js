import router from '@system.router';
import prompt from '@system.prompt';
import ability_featureAbility from '@ohos.ability.featureAbility';
import data_storage from '@ohos.data.storage';

class Note{
    constructor(title,group,time_stamp){
        this.title = title;
        this.group = group;
        this.time_stamp = time_stamp;
        this.is_selected = false;
    }
}
class Group{
    constructor(name,color){
        this.name = name;
        this.color = color;
    }
}

export default {
    data: {
        title:'笔记本名称',
        text:'笔记本简介',
        notebook_id:0,
        time_stamp:'0',
        space:' ',          //色块留白区
        color_list:[        //分区颜色选项
            {
                name:'纯净白',
                color:'white'
            },
            {
                name:'可可棕',
                color: '#b15716'
            },
            {
                name:'海军蓝',
                color: '#000080'
            },
            {
                name:'紫罗兰',
                color:'#FF8A2BE2'
            },
            {
                name:'生机绿',
                color: '#FF6EFF00'
            },
            {
                name:'战舰灰',
                color:'#FF7694AC'
            },
            {
                name:'魅力红',
                color: '#FFFF0000'
            },
        ],
        group_list:[],
        group_list_init:[
            new Group('默认分组','white'),
            new Group('分组1','#FF8A2BE2'),
            new Group('分组2', '#000080'),
        ],
        note_list:[],
        note_list_init:[
            new Note('笔记1','默认分组','8086'),
            new Note('笔记2','默认分组','8051'),
            new Note('笔记3','分组1','1038'),
            new Note('笔记4','分组1','4532'),
            new Note('笔记5','分组2','2415'),
            new Note('笔记6','分组2','2426'),
        ],
        is_multi_select:false,           //多选状态参量，为true时代表处于多选状态
        note_selected:[],                //多选状态下被选中的笔记列表，存放被选中笔记在note_list中的索引，初始为空
        new_note:new Note('','默认分组','0'),  //新添加的笔记对象
        new_group:new Group('','white'), //新增分组对象
        is_edit:false,                   //编辑状态参量，为true时代表正在修改信息
        target_note:0,                   //索引，用于修改笔记属性
        target_group:0,                  //索引，用于修改分组属性
        my_storage:null,
        dir:'',
        path:'',
    },
    async onInit(){
        //建立轻量级偏好数据库存储接口
        var context = ability_featureAbility.getContext();
        this.dir = await context.getFilesDir();
        this.path = this.dir + '/' + this.time_stamp;
        this.my_storage = data_storage.getStorageSync(this.path);
        //从数据库中恢复数据
        if(this.my_storage.hasSync('group_list')){
            let value = this.my_storage.getSync('group_list','default');
            this.group_list = JSON.parse(value);
        }else{
            this.group_list = this.group_list_init;
        }
        if(this.my_storage.hasSync('note_list')){
            let value = this.my_storage.getSync('note_list','default');
            this.note_list = JSON.parse(value);
        }else{
            this.note_list = this.note_list_init;
        }
        for(let note of this.note_list){
            if(!this.my_storage.hasSync(note.time_stamp))
            this.my_storage.putSync(note.time_stamp,'化繁为简，享你所记');
        }this.my_storage.flushSync();
    },
    set_storage_note() {            //将当前笔记列表保存至storage中
        this.my_storage.putSync('note_list',JSON.stringify(this.note_list));
        this.my_storage.flushSync();
    },
    set_storage_group() {            //将当前分组列表保存至storage中
        this.my_storage.putSync('group_list',JSON.stringify(this.group_list));
        this.my_storage.flushSync();
    },
    onDestroy(){

    },
    onBackPress(){
        if(this.is_multi_select){
            this.exit_multi_select();
        }else{
            this.set_storage_note();
            this.set_storage_group();
            router.back();
        }
        return true;
    },
    to_editor(id){
        if(!this.is_multi_select){
            router.push({uri:'pages/editor/editor',
                params:
                {
                    group:this.note_list[id].group,
                    title:this.note_list[id].title,
                    notebook_stamp:this.time_stamp,
                    note_key:this.note_list[id].time_stamp
                }
            });
        }
    },
//新增操作：新增笔记和新建分组
    add_selected(e){
        switch(e.value){
            case 'add_note':this.add_note();break;     //新增笔记
            case 'add_group':this.add_group();break;   //新建分组
        }
    },
    //函数集：新增笔记和编辑笔记信息
    add_note() {
        this.$element('add_dialog').show(); //选择新增笔记，弹窗收集添加笔记本相关信息
    },
    cancel_add(){
        this.new_notebook = new Note('','默认分组','0');     //取消添加，清空新增笔记对象
        this.new_group =  new Group('','white');   //取消添加，清空新增分组对象
        this.target_note = 0;
        this.target_group = 0;
        this.is_edit = false;                          //取消编辑，复位编辑状态参量
    },
    //获取新增笔记本的标题和分组，保存在新增笔记对象中
    get_note_title(e){
        this.new_note.title = e.value;
    },
    get_note_group(e){
        this.new_note.group = e.newValue;
    },
    done_add(){                             //确认添加/编辑，将新增笔记对象添加/保存至note_list中
        if(!this.new_note.title){
            prompt.showToast({
                message: '请输入笔记标题',                     //提示输入笔记标题
                duration: 3000,
            });
        } else if(this.is_edit){          //若当前处于编辑状态，则执行编辑操作
            for (let note of this.note_list) {             //检测是否同分组下重名
                if(
                (
                    this.new_note.title !=this.note_list[this.target_note].title     //排除编辑状态下未作修改的情况
                    || this.new_note.group != this.note_list[this.target_note].group
                )
                && this.new_note.title == note.title && this.new_note.group == note.group
                ) {
                    prompt.showToast({
                        message: '该分组下已有同名笔记',
                        duration: 3000,
                    });
                    return;
                }
            }
            //将被修改笔记对象更新为新增笔记对象，分开赋值以避免浅拷贝
            this.note_list[this.target_note].title = this.new_note.title;
            this.note_list[this.target_note].group = this.new_note.group;
            this.is_edit = false; //编辑完成，复位编辑状态参量
        } else {                             //若当前未处于编辑状态，则执行添加操作
            for (let note of this.note_list) {             //检测是否同分组下重名
                if(this.new_note.title == note.title && this.new_note.group == note.group){
                    prompt.showToast({
                        message: '同分组下已有同名笔记',
                        duration: 3000,
                    });
                    return;
                }
            }
            let new_date = new Date();                    //为新增笔记对象盖戳
            let new_time_stamp = new_date.toString();
            this.new_note.time_stamp = new_time_stamp;
            this.note_list.push(this.new_note);           //将新增笔记对象加入数组
            this.my_storage.putSync(this.new_note.time_stamp,'化繁为简，享你所记'); //初始化笔记数据
        }
        this.$element('add_dialog').close();
        this.set_storage_note();
        this.new_note = new Note('', '默认分组','0'); //执行完成，清空新增笔记对象
        this.target_note = 0;                       //执行完成，复位目标索引
    },
    //函数集：新增和修改分组属性
    add_group(){
        this.$element('add_group').show();                  //选择新增分组，弹窗收集分组名称
    },
    get_group_color(e){
        this.new_group.color = e.newValue;
    },
    get_group_name(e){
        this.new_group.name = e.value;
    },
    set_new_group(){
        if(!this.new_group.name){
            prompt.showToast({
                message: '请输入分组名称',                     //提示输入分组名称
                duration: 3000,
            });
        } else { //若当前处于编辑状态，则执行编辑操作
            for (let group of this.group_list) {           //检测是否重名
                if (
                (this.group_list[this.target_group].name != this.new_group.name //排除编辑状态下未作修改的情况
                && this.new_group.name == group.name)
                || this.new_group.name == this.group_list[0].name //不允许与默认分组同名
                ) {
                    prompt.showToast({
                        message: '请勿创建重名分组',
                        duration: 3000,
                    });
                    return;
                }
            }
            if (this.is_edit) {
                for (let note of this.note_list) {         //更新目标分组中的笔记的分组名称
                    if (note.group == this.group_list[this.target_group].name) {
                        note.group = this.new_group.name;
                    }
                }
                this.group_list.splice(this.target_group, 1, this.new_group); //更新数组中的分组对象
                this.is_edit = false;                     //编辑完成，复位编辑状态参量
            } else {                                      //若当前未处于编辑状态，则执行添加操作
                this.group_list.push(this.new_group);
            }
            this.$element('add_group').close();
            this.set_storage_group();
            this.new_group = new Group('', 'white');     //执行完成，清空新增分组名称
            this.target_group = 0;                       //执行完成，复位目标索引
        }
    },

//笔记操作：信息编辑和批量操作
    operation_note(e){                                     //非多选状态下长按弹出笔记操作菜单
        if(!this.is_multi_select){
            this.target_note = e.detail.note_id;           //更新索引
            prompt.showDialog({                            //弹出操作菜单
                title:'提示',
                message:'选择操作',
                buttons:[
                    {
                        text:'修改属性',
                        color:'green'
                    },
                    {
                        text:'进入多选',
                        color:'blue'
                    },
                ],
                success:(data) => {                            //执行菜单操作
                    switch(data.index){
                        case 0:this.edit_note_item();break;
                        case 1:this.into_multi_select();break;
                    }
                },
                cancel:function(){
                },
            });
        }
    },
    edit_note_item(){                                     //编辑笔记标题和分组
        //利用新增笔记对象暂存待编辑信息，分开赋值以避免浅拷贝
        this.new_note.title = this.note_list[this.target_note].title;
        this.new_note.group = this.note_list[this.target_note].group;
        this.is_edit = true;                             //置位编辑状态符
        this.$element('add_dialog').show();              //借用新增笔记对话框进行信息编辑
    },
    //函数集：笔记的多选操作-批量删除和移动
    into_multi_select(){                                 //进入多选状态
        this.is_multi_select = true;                     //置位多选状态参量
    },
    end_multi_operation(){                               //结束批量操作
        this.note_selected = [];                         //清空被选中列表
        for(let note of this.note_list){
            note.is_selected = false;                    //遍历并复位所有笔记对象的被选状态参量
        }
    },
    exit_multi_select(){                                 //退出多选模式
        this.is_multi_select = false;                    //复位多选状态符
        this.note_selected = [];                         //清空被选笔记列表
        for(let i in this.note_list){
            this.note_list[i].is_selected = false;       //遍历并复位所有笔记对象的被选状态参量
        }
    },
    select(e){                                           //由note_item传入的点击事件：变更被选状态
        let note_id = e.detail.note_id;                              //获取被点击的notebook列表索引
        this.note_list[note_id].is_selected =!this.note_list[note_id].is_selected;  //变更被选状态（选中<->未选中）
        if(this.note_list[note_id].is_selected==true){               //选中状态为true，表示被选中，加入被选中列表
            this.note_selected.splice(note_id,0,note_id);
        }else{                                                       //选中状态为false，表示取消选中，从被选中列表中去除
            let index = this.note_selected.findIndex(item=>item == note_id);    //用查找——修改方法去除目标元素
            this.note_selected.splice(index,1);
        }
        this.note_selected = this.note_selected.sort((a,b)=>a-b); //将note_selected中的元素（被选中notebook索引）从小到大排序
    },
    //批量删除笔记
    delete_note(){                                   //多选后按下右上角删除键，弹窗确认是否删除
        if(this.note_selected.length != 0){                      //有note被选中时才弹窗
            this.$element('delete_dialog').show();
        }
    },
    done_delete() {                                 //在确认删除弹窗中按下确认，执行删除操作
        this.$element('delete_dialog').close();
        let after_list = [];
        for(let i of this.note_list){               //获取未被选中的note，组成数组after_list
            if(i.is_selected == false){
                after_list.push(i);
            }
        }
        for(let index of this.note_selected){
            this.my_storage.deleteSync(this.note_list[index].time_stamp)  //删除对应笔记数据
        }
        this.end_multi_operation();                 //结束多选操作
        this.note_list = after_list;                //将note_list更新为不含被删除对象的after_list
        this.set_storage_note();
    },
    //批量移动笔记
    more_note(){                                    //多选后按下右上角“更多”键，弹窗确认选择移动操作
        if(this.note_selected.length != 0){
            this.$element("more-menu").show({x:360,y:30});
        }
    },
    more_selected(e){
        switch(e.value){
            case 'move_note':this.move_note();break;
        }
    },
    move_note(){
        this.$element('move_dialog').show();
    },
    get_move_group(e){
        this.new_group.name = e.newValue;          //借用新增分组对象保存目标分组名称
    },
    cancel_move(){
        this.new_group.name = '';
    },
    done_move(){
        for(let i of this.note_selected){         //检测重名
            for(let note of this.note_list){
                if(note.group == this.new_group.name && note.title == this.note_list[i].title){
                    prompt.showToast({
                        message:"目标分组中已存在同名笔记"
                    });
                    return;
                }
            }
        }
        this.$element('move_dialog').close();
        if(this.new_group.name){
            for(let note of this.note_list){      //更新笔记的分组名称
                if(note.is_selected){
                    note.group = this.new_group.name;
                }
            }
        }
        this.new_group.name = '';                //清空新增分组名称
        this.end_multi_operation();              //结束多选操作
        this.set_storage_note();
    },

//分组操作：更改分组属性和解散分组
    operation_group(index){                             //长按弹出分组操作菜单
            if(index!=0){                                   //默认分组不允许修改
                this.target_group = index;                  //更新目标分组索引
                prompt.showDialog({                         //弹出操作菜单
                    title:'提示',
                    message:'选择操作',
                    buttons:[
                        {
                            text:'修改分组',
                            color:'blue'
                        },
                        {
                            text:'解散分组',
                            color:'red'
                        },
                    ],
                    success:(data) => {                     //执行菜单操作
                        switch(data.index){
                            case 0:this.edit_group();break;
                            case 1:this.remove_group();break;
                        }
                    },
                    cancel:function(){
                    },
                });
        }
    },
    edit_group(){                            //编辑分组颜色和名称
        //利用新增分组对象暂存待编辑信息
        this.new_group.name = this.group_list[this.target_group].name;
        this.new_group.color = this.group_list[this.target_group].color;
        this.is_edit = true;                 //置位编辑状态符
        this.$element('add_group').show();   //借用新增分组对话框进行编辑
    },
    remove_group(){
        prompt.showDialog({
            title:'解散分组',
            message:'其下笔记将移至默认分组',
            buttons:[
                {
                    text:'删除',
                    color:'red'
                },
                {
                    text:'取消',
                    color:'blue'
                },
            ],
            success:(data) => {
                if(data.index == 0){
                    for(let noteA of this.note_list){    //检测待解散分组中的笔记是否与默认分组中有重名
                        if(noteA.group == this.group_list[0].name){
                            for(let noteB of this.note_list){
                                if(
                                    noteB.group == this.group_list[this.target_group].name
                                    && noteB.title == noteA.title
                                ){
                                    prompt.showToast({
                                        message:"请先移除重名笔记"
                                    });
                                    return;
                                }
                            }
                        }
                    }
                    this.done_remove();
                    this.set_storage_group();
                }
            },
            cancel:function(){
            },
        });
    },
    done_remove(){
        for(let note of this.note_list){
            if(note.group == this.group_list[this.target_group].name){
                note.group = this.group_list[0].name;              //将其下的笔记移至默认分组
            }
        }
        this.group_list.splice(this.target_group,1);               //将目标分组从group_list中移除
    }
}
