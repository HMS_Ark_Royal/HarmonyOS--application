import prompt from '@system.prompt';

const TAG = '[note_item]';

export default {
    props: {
        multiSelectMode: {       //多选状态参量
            default: false
        },
        noteId:{                 //当前note组件的列表索引
            default:0
        },
        isSelected:{             //被选状态参量,由fragment_main传入，为true表示被选中
            default:false
        },
        noteTitle:{         //notebook名称

        },
        noteGroup:{
            default:'default'
        }
    },
    data:{

    },
    onInit() {

    },
    onReady() {

    },
    onShow() {
        console.log(TAG + 'onShow');
    },
    onDestroy() {
        console.log(TAG + 'onDestroy');
    },


    choose_menu(){
        this.$emit('choose_operation',{note_id:this.noteId});  //长按弹出操作菜单
    },
    select(){                       //点击事件：变更被选状态（选中<->未选中）
        this.$emit('note_selected', {note_id:this.noteId});
    },
}