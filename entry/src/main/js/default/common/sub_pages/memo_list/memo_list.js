import prompt from '@system.prompt';
import ability_featureAbility from '@ohos.ability.featureAbility';
import data_storage from '@ohos.data.storage';

export default {
    data: {
        input_content:``,
        memo_list:[           //便签列表
            {
                content:'曲高未必人不识，自有知音和清词',
                is_todo:false,
                is_done:false
            },
            {
                content:`欲买桂花同载酒\n终不似\n少年游`,
                is_todo:false,
                is_done:false
            },
            {
                content:'运动半小时',
                is_todo:true,
                is_done:true
            },
            {
                content:'看书',
                is_todo:true,
                is_done:false
            }
        ],
        target:0,                //便签操作索引
        text_in_edit:'',
        my_storage:null,
        path:''
    },

    async onInit() {
        //建立轻量级偏好数据库存储接口
        var context = ability_featureAbility.getContext()
        this.path = await context.getFilesDir() + '/memo'
        this.my_storage = data_storage.getStorageSync(this.path);
        //从数据库中恢复数据
        if(this.my_storage.hasSync('memo_list')){
            let value = this.my_storage.getSync('memo_list','default');
            this.memo_list = JSON.parse(value);
        }
    },
    onDestroy() {
        this.setStorage();
    },
    setStorage() {            //将当前便签列表保存至storage中
        this.my_storage.putSync('memo_list',JSON.stringify(this.memo_list));
        this.my_storage.flushSync();
    },
    //利用输入框新增便签
    get_memo(e){
        this.input_content = e.value;
    },
    add_memo(){
        if(!this.input_content)return prompt.showToast({
            message:'请输入内容'
        })
        this.memo_list.push(
            {
                content:this.input_content,
                is_todo:false,
                is_done:false
            }
        );
        this.input_content = '';                  //清空输入值
        this.setStorage();
    },
    //改变待办完成状态（未完成<->已完成）
    change_status(e){
        let index=e.detail.memo_id;               //由自定义组件事件传入该操作便签索引
        this.memo_list[index].is_done = !this.memo_list[index].is_done;  //翻转便签项的完成标识符
        this.setStorage();
    },
    //长按操作菜单
    operate_memo(e){
            this.target = e.detail.memo_id;      //更新便签操作索引
            this.$element("memo_operate_menu").show({x:250,y:600});  //显示操作菜单
    },
    //执行菜单选项
    do_operate(e){
        let operate = e.value;
        switch(operate){
            case 'edit':this.memo_into_edit();break;
            case 'toggle_todo':this.memo_toggle_state();break;
            case 'delete':this.delete_memo();break;
        }
    },
    //在弹窗中编辑便签/待办内容
    memo_into_edit(){
        this.$element('edit_dialog').show();
    },
    cancel_edit(){
        this.text_in_edit = '';      //编辑取消。清空编辑值
    },
    edit_memo(e){
        this.text_in_edit = e.value;//获取编辑值
    },
    //便签项结束编辑
    memo_end_edit(){
        this.$element('edit_dialog').close();
        if(this.text_in_edit)
        this.memo_list[this.target].content = this.text_in_edit;  //若有进行编辑操作，更新便签项内容
        this.setStorage();
        this.cancel_edit();
    },
    //切换便签项类型（便签<->待办）
    memo_toggle_state(){
        this.memo_list[this.target].is_todo = !this.memo_list[this.target].is_todo;  //翻转便签项的待办标识符
        this.memo_list[this.target].is_done = false;                                 //复位便签项的完成标识符
        this.setStorage();

    },
    //删除便签项，利用对话框确认
    delete_memo(){
        prompt.showDialog({
            title:'提示',
            message:'确认删除？',
            buttons:[
                {
                    text:'删除',
                    color:'red'
                },
                {
                    text:'取消',
                    color:'blue'
                },
            ],
            success:(data) => {
                if(data.index == 0){
                    this.memo_list.splice(this.target,1);
                    this.setStorage();
                }
            },
            cancel:function(){
            },
        });
    }

}