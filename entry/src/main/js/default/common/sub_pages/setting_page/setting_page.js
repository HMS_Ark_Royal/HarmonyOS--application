import prompt from '@system.prompt';

const TAG = '[setting_page]';

export default {
    data: {

    },
    onInit() {
        console.log(TAG + 'onInit');
    },
    onReady() {
        console.log(TAG + 'onReady');
    },
    onShow() {
        console.log(TAG + 'onShow');
    },
    onDestroy() {
        console.log(TAG + 'onDestroy');
    },

    setting_change(e){
        if(e.checked){
            prompt.showToast({
                message: this.$t('strings.wait_updates')
            });
        }else{
            prompt.showToast({
                message: this.$t('strings.thanks')
            });
        }
    }
}