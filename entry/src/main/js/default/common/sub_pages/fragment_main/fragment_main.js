import prompt from '@system.prompt';
import ability_featureAbility from '@ohos.ability.featureAbility';
import data_storage from '@ohos.data.storage';
import router from '@system.router';

class Notebook{
    constructor(icon,name,description,time_stamp){
        this.notebook_name = name;
        this.notebook_description = description;
        this.is_selected = false;
        this.time_stamp = time_stamp;      //时间戳，作为每个笔记本对象建立轻量级数据库的唯一标识
        switch(icon){
            case 1:
                this.notebook_icon = "/common/images/notebook_icon/notebook_icon1.png";
                break;
            case 2:
                this.notebook_icon = "/common/images/notebook_icon/notebook_icon2.png";
                break;
            case 3:
                this.notebook_icon = "/common/images/notebook_icon/notebook_icon3.png";
                break;
            case 4:
                this.notebook_icon = "/common/images/notebook_icon/notebook_icon4.png";
                break;
            case 5:
                this.notebook_icon = "/common/images/notebook_icon/notebook_icon5.png";
                break;
            default:
                this.notebook_icon = "/common/images/notebook_icon/notebook_icon1.png";
        }
    }
}

export default {
    props: {
        multiSelectMode: {
            default: false   //多选状态参量，由index页面传入
        },
    },
    data: {
        list_data:[],
        list_data_init: [        //用于显示的笔记本列表
            new Notebook(1,'我的笔记','一本简单而实用的笔记','1649'),
            new Notebook(2,'美文收集','积累阅读中遇到的小美好','3668'),
            new Notebook(3,'灵感闪记','脑海中那一闪而过的璀璨','5835'),
            new Notebook(4,'工作事项','为全人类的解放而奋斗','1145'),
            new Notebook(5,'生活随笔','你所热爱的，就是你的生活','1419')
            ],
        initial_index_value: 0,
        notebook_selected:[],     //多选状态下被选中的笔记本列表，初始为空
        new_notebook:new Notebook(1,'','','0'),  //新添加的笔记本对象，初始为空
        is_edit:false,            //状态参量，为true时代表正在编辑笔记本信息
        target:0,                  //编辑笔记本信息操作索引
        my_storage:null,
        dir:'',
        path:'',
    },
    async onInit() {
        //建立轻量级偏好数据库存储接口
        var context = ability_featureAbility.getContext();
        this.dir = await context.getFilesDir();
        this.path = this.dir+'/notebook_list';
        this.my_storage = data_storage.getStorageSync(this.path);
        //从数据库中恢复数据
        if(this.my_storage.hasSync('list_data')){
            let value = this.my_storage.getSync('list_data','default');
            this.list_data = JSON.parse(value);
        }else{
            this.list_data = this.list_data_init;
        }
        //为每一个笔记本创建轻量级数据库
        for(let notebook of this.list_data_init){
            let storage = data_storage.getStorageSync(this.dir + '/' + notebook.time_stamp);
            data_storage.removeStorageFromCacheSync(this.dir + '/' + notebook.time_stamp);
        }
        this.$watch('multiSelectMode','exit_multi_select');  //监听多选状态参量
    },
    setStorage() {            //将当前笔记本列表保存至storage中
        this.my_storage.putSync('list_data',JSON.stringify(this.list_data));
        this.my_storage.flushSync();
    },
    onDestroy() {
        this.setStorage();
    },

//    enterkeyClick(e) {
//        prompt.showToast({
//            message: "enter key clicked",
//            duration: 3000,
//        });
//    },
//    getFocusOfSearch(e) {
//        this.startSearch = 1;
//    },
//    searchChange(e) {
//        if (startSearch != 0) {
//            prompt.showToast({
//                message: "value: " + e.value,
//                duration: 3000,
//            });
//        }
//    },
    to_notes($idx){
        if(!this.multiSelectMode)
        router.push({uri:'pages/notes/notes',
            params:
            {
                notebook_id:$idx,
                title:this.list_data[$idx].notebook_name,
                text:this.list_data[$idx].notebook_description,
                time_stamp:this.list_data[$idx].time_stamp
            }
        });
    },
    add_notebook() {
        this.$element('add_dialog').show(); //按下新增键，弹窗收集添加笔记本相关信息
    },
    cancel_add(){
        this.new_notebook =new Notebook(1,'','');  //取消添加，清空新增笔记本对象
        this.is_edit = false;                      //取消编辑，复位编辑状态参量
    },
    //获取新增笔记本的名称、图标和简介，保存在新增笔记本对象中
    get_notebook_name(e){
        this.new_notebook.notebook_name = e.value;
    },
    get_notebook_icon(e){
        this.new_notebook.notebook_icon = e.newValue;
    },
    get_notebook_description(e){
        this.new_notebook.notebook_description = e.text;
    },
    //确认添加/编辑，将新增笔记本对象添加至list_data中
    done_add(){
        if(!this.new_notebook.notebook_name){
            prompt.showToast({
                message: '请输入笔记本名称',                     //提示输入笔记本名称
                duration: 3000,
            });
        }else{
            for(let notebook of this.list_data) {           //检测是否重名
                if(
                    (this.list_data[this.target].notebook_name != this.new_notebook.notebook_name //排除编辑状态下未作修改的情况
                    && this.new_notebook.notebook_name == notebook.notebook_name)
                    || this.new_notebook.notebook_name == this.list_data[0].notebook_name //不允许与默认target同名
                ){
                    prompt.showToast({
                        message: '请勿创建重名记事本',
                        duration: 3000,
                    });
                    return;
                }
            }
            this.$element('add_dialog').close();
            if(this.is_edit){ //若当前处于编辑状态，则执行编辑操作
                //将被编辑笔记本对象更新为新增笔记本对象，分开赋值以避免浅拷贝
                this.list_data[this.target].notebook_name = this.new_notebook.notebook_name;
                this.list_data[this.target].notebook_description = this.new_notebook.notebook_description;
                this.list_data[this.target].notebook_icon = this.new_notebook.notebook_icon;
                this.is_edit = false;                         //编辑完成，复位编辑状态参量
            }else{                                            //若当前未处于编辑状态，则执行添加操作
                let new_date = new Date();                    //为新增笔记本对象盖戳
                let new_time_stamp = new_date.toString();
                this.new_notebook.time_stamp = new_time_stamp;
                this.list_data.push(this.new_notebook);       //将新增笔记本对象加入数组
                //创建对应目录
            }
            this.new_notebook =new Notebook(1,'','','0');         //执行完成，清空新增笔记本对象
            this.target = 0;
            this.setStorage();
        }
    },

    operation_menu(e){
        this.target = e.detail.note_id;      //更新编辑操作索引
        prompt.showDialog({                  //弹出操作菜单
            title:'提示',
            message:'选择操作',
            buttons:[
                {
                    text:'编辑信息',
                    color:'#0df518'
                },
                {
                    text:'进入多选',
                    color:'blue'
                },
            ],
            success:(data) => {             //执行菜单操作
                switch(data.index){
                    case 0:this.edit_notebook_info();break;
                    case 1:this.into_multi_select();break;
                }
            },
            cancel:function(){
            },
        });
    },
    edit_notebook_info(){                      //编辑笔记本信息
        //利用新增笔记本对象暂存待编辑信息，分开赋值以避免浅拷贝
        this.new_notebook.notebook_name = this.list_data[this.target].notebook_name;
        this.new_notebook.notebook_description = this.list_data[this.target].notebook_description;
        this.new_notebook.notebook_icon = this.list_data[this.target].notebook_icon;
        this.is_edit = true;                  //置位编辑状态符
        this.$element('add_dialog').show();   //借用新增对话框进行信息编辑
    },
    into_multi_select(){
        this.$emit('multiSelect');            //向index传递：进入多选状态
    },
    exit_multi_select(newV,oldV){
        if(newV == false && oldV == true){
            this.notebook_selected = [];      //多选状态参量由true变为false时退出多选状态：清空被选中列表
            for(let i in this.list_data){
                this.list_data[i].is_selected = false;                    //遍历并复位所有笔记本对象的被选状态参量
            }
        }
    },
    select(e){                                //由notebook_item传入的点击事件：变更被选状态
        let notebook_id = e.detail.note_id;                              //获取被点击的notebook列表索引
        this.list_data[notebook_id].is_selected =!this.list_data[notebook_id].is_selected;  //变更被选状态（选中<->未选中）
        if(this.list_data[notebook_id].is_selected==true){               //选中状态为true，表示被选中，加入被选中列表
            this.notebook_selected.splice(notebook_id,0,notebook_id);
        }else{                                                           //选中状态为false，表示取消选中，从被选中列表中去除
            let index = this.notebook_selected.findIndex(item=>item == notebook_id);        //用查找——修改方法去除目标元素
            this.notebook_selected.splice(index,1);
        }
        this.notebook_selected =this.notebook_selected.sort((a,b)=>a-b); //将notebook_selected中的元素（被选中notebook索引）从小到大排序
    },
    delete_notebook(){                        //多选后按下删除键，弹窗确认是否删除
        if(this.notebook_selected.length != 0){                          //有notebook被选中时才弹窗
            this.$element('delete_dialog').show();
        }
    },
    done_delete() {                           //在确认删除弹窗中按下确认，执行删除操作
        this.$element('delete_dialog').close();
        let after_list = [];
        for(let i of this.list_data){
            if(i.is_selected == false){
                after_list.push(i);           //获取未被选中的notebook，组成数组
            }
        }
        for(let index of this.notebook_selected){    //删除对应的轻量级数据库
            data_storage.deleteStorageSync(this.dir + '/'+this.list_data[index].time_stamp);
        }
        this.$emit('delete');                 //将事件传递至index，退出多选状态
        this.list_data = after_list;          //将notebook的list_data更新为不含被删除对象的after_list
        this.setStorage();
    },

    more_notebook() {
        prompt.showToast({
            message: 'more',
            duration: 3000,
        });
    },
}