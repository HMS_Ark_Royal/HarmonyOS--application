export default {
    props: {
        text: {      //便签文字内容
            default:''
        },
        isTodo:{     //便签待办标识
            default:false
        },
        isDone:{     //待办已完成标识
            default:false
        },
        memoId:{     //便签索引

        },

    },
    data:{
        text_style:"common-text",  //便签文字初始样式
    },

    onInit() {
        this.$watch('isDone','refresh')  //监听已完成标识符，动态刷新文字样式
        if(this.isDone){                 //确定当前文字样式
            this.text_style = "done-text";
        }else{
            this.text_style = "common-text";
        }
    },
    refresh(newV,oldV){                //根据已完成标识符的变化，动态刷新文字样式
        if(newV==true && oldV==false){
            this.text_style = 'done-text';
        }
        else if(newV==false && oldV==true){
            this.text_style = 'common-text';
        }
    },
    change_todo_state(){               //用户手动对check组件进行勾选/消钩：改变已完成状态
        this.$emit('changeTodoState', {memo_id:this.memoId});
    },
    operate_menu(){                   //用户长按便签项：呼出操作菜单
        this.$emit('operateMenu',{memo_id:this.memoId});
    }
}