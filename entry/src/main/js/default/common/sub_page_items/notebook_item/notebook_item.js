import prompt from '@system.prompt';

const TAG = '[notebook_item]';

export default {
    props: {
        multiSelectMode: {       //多选状态参量，由index经fragment_main传入
            default: false
        },
        noteId:{                 //当前notebook组件的列表索引，由fragment_main传入
            default:0
        },
        isSelected:{             //被选状态参量,由fragment_main传入，为true表示被选中
            default:false
        },
        //notebook相关信息，由fragment_main传入
        notebook_icon:{         //notebook图标

        },
        notebook_name:{         //notebook名称

        },
        notebook_description:{  //notebook简介

        }
    },
    data:{

    },
    onInit() {

    },
    onReady() {

    },
    onShow() {
        console.log(TAG + 'onShow');
    },
    onDestroy() {
        console.log(TAG + 'onDestroy');
    },


    choose_menu(){
        this.$emit('choose_operation',{note_id:this.noteId});  //长按弹出操作菜单
    },
    select(){                       //点击事件：变更被选状态（选中<->未选中）
        this.$emit('notebook_selected', {note_id:this.noteId}); //将事件以及当前notebook的被选状态和列表索引向fragment_main传递
    },
}