# 鸿蒙应用：简记

#### 介绍
毕业设计：基于鸿蒙系统的记事本应用，包含笔记和便签-备忘功能

#### 软件架构
本应用使用Ark UI框架进行JS UI开发

主页（index）为list-tab框架，各功能页面以自定义组件的形式作为子页面插入至index的tab-content中，其代码存放于/common/sub_pages目录下



#### 安装教程

1.  使用DevEco Studio导入工程
2.  按照需要进行虚拟机或真机运行

#### 使用说明

1. 笔记本管理页面功能：新增笔记本、编辑笔记本信息、批量选择并删除笔记本
2. 笔记展示页面功能：
   1. 笔记操作：新增笔记、设置笔记分组、编辑笔记标题、批量移动或删除笔记
   2. 分组操作：新增分组、修改分组名称和颜色、解散分组
3. 笔记编辑页面功能：笔记查看、编辑和保存
4. 便签-备忘功能：新增便签、编辑便签、删除便签、设为备忘、标记为已完成
5. 更多功能正在开发中

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
